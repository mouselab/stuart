#!/bin/sh

#SBATCH -J "after"
#SBATCH --qos=normal
#SBATCH --mem=1000
#SBATCH -o after.out -e after.err
#SBATCH --mail-type=END  --mail-user=mbourdon@pasteur.fr

Rscript after.R  || exit 1


exit 0
