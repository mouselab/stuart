#!/bin/sh

#SBATCH -J "before_after"
#SBATCH --qos=normal
#SBATCH --mem=1000
#SBATCH -o before_after.out -e before_after.err
#SBATCH --mail-type=END  --mail-user=mbourdon@pasteur.fr

Rscript before_after.R  || exit 1


exit 0
