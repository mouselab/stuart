find_linked_markers <- function(rf,annot,mark,maxit=10000,tol=1e-6,lod=3){
  #calculate matrix with estimated RF

  names <- rownames(rf)  
  estrf_df <- as_tibble(rf)
  estrf_df <- tibble(marker=names,
                     estrf_df)
  
  
  estrf_df %<>% select(marker,all_of(mark))
  estrf_df <- full_join(estrf_df,annot,by=c("marker"="marker"))
  estrf_df %>% filter(!!sym(mark)>3)
}