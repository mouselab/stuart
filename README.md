
<!-- README.md is generated from README.Rmd. Please edit that file -->

# stuart

<!-- badges: start -->
<!-- badges: end -->

stuart is a R package used to analyze whole genome genotyping results of
animals used by crossing laboratory strains. It is particularly useful
for F2 or N2 individuals as it allows to filter the markers in the
arrays that can or cannot be used for further analysis from a genetic
point of view. Mrkers will be selected depending on their proportion of
each genotype, correspondance between F2 or N2 individuals alleles and
parental strains alleles, etc.

## Installation

You can install the released version of stuart from GitLab, directly in
R with:

`devtools::install_gitlab(repo="mouselab/stuart",host="gitlab.pasteur.fr")`

## Example

To see an example of the use of stuart package with miniMUGA array,
consult the package vignette.
