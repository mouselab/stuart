#' @title Create data frame in Rqtl CSV format
#'
#' @description This function uses the table produced by tab_mark function filled by all the mark_* functions in order to create a data frame in the right format for Rqtl read.cross function. Only the non-excluded markers will be kept and genotypeds will be encoded in "0", "1" and "2", "0" being homozygous for the first parental strain, "1" heterozygous and "2" homozygous for the second parental strain. Caution, this file create a data frame and a CSV file in the path of your choice if indicated by the "path" argument. This function does not create a "cross" object in your environment that can be directly used for QTL mapping. You will need to load the CSV file with qtl::read.cross.
#' @param geno data frame with the genotyping results for your cross
#' @param pheno data frame with phenotypes of the individuals (individuals must have the same ID in the geno data frame and in the pheno data frame)
#' @param prefix potential prefix present in the names of the individuals in the geno data frame to be removed in ordere to have the same names as in the pheno file
#' @param tab data frame obtained with tab_mark function
#' @param ref data frame with the reference genotypes of mouse lines
#' @param par1 first parental strain used in the cross, the name must be written as in the "ref" data frame
#' @param par2 second parental strain used in the cross, the name must be written as in the "ref" data frame
#' @param pos column with marker positions
#' @param path if indicated, the data frame will be exported in this path
#'
#' @import dplyr
#' @import tidyr
#' @import utils
#' @import stringr
#'
#' @export
#'
#### write_rqtl ####
## write data frame in rqtl format (csv), if path != NA writes the file in the path indicated
write_rqtl <- function(geno,pheno,tab,ref,par1,par2,prefix,pos,path=NA){
  #rename df columns
  geno <- geno %>% rename("marker"=1,
                          "id"=2,
                          "allele_1"=3,
                          "allele_2"=4)

  #extract snps non excluded
  if("exclude_match" %in% colnames(tab)){
    tab <- tab %>% filter(exclude_match==0)
  }

  if("exclude_poly" %in% colnames(tab)){
    tab <- tab %>% filter(exclude_poly==0)
  }

  if("exclude_prop" %in% colnames(tab)){
    tab <- tab %>% filter(exclude_prop==0)
  }

  if("exclude_allele" %in% colnames(tab)){
    tab <- tab %>% filter(exclude_allele==0)
  }

  if("exclude_estmap" %in% colnames(tab)){
    tab <- tab %>% filter(exclude_estmap==0)
  }


  #filter genotypes for non excluded markers in geno file
  geno <- geno %>% select(c(marker,id,allele_1,allele_2)) %>% filter(marker %in% tab$marker)

  #recode parents' names to match column names nomenclature
  par1 <- make.names(par1)
  par2 <- make.names(par2)

  #keep parental lines genotypes
  colnames(ref) <- make.names(colnames(ref))
  ref <- ref %>% select(marker,chr,!!sym(pos),!!sym(par1),!!sym(par2))

  #merge genotypes with parents
  geno <- left_join(geno,ref,by=c("marker"="marker"))

  #remove snps with no position
  geno <- geno %>% filter(is.na(chr)==FALSE) %>% filter(is.na(!!sym(pos))==FALSE)

  #recode "-" in "N" in geno file
  geno <- geno %>% mutate(allele_1 = recode(allele_1,
                                            "-" = "N"))

  geno <- geno %>% mutate(allele_2 = recode(allele_2,
                                            "-" = "N"))

  #recode geno in factors with same levels
  geno <- geno %>% mutate(allele_1 = factor(allele_1,levels=c("A","C","G","H","N","T")))
  geno <- geno %>% mutate(allele_2 = factor(allele_2,levels=c("A","C","G","H","N","T")))



  #recode genotypes depending on parents' genotypes
  geno <- geno %>% mutate(Geno = case_when(
    #if one allele not genotyped:
    allele_1=="N" | allele_2=="N" ~ "NA",

    #if both alleles genotyped
    ##homozygous 0
    allele_1==allele_2 & allele_1==!!sym(par1) ~ "0",
    ##homozygous 2
    allele_1==allele_2 & allele_1==!!sym(par2) ~ "2",
    ##heterozygous
    allele_1!=allele_2 ~ "1",

    #if parental strains are N/H
    ##homozygous for parent that is N/H
    ###homozygous 0
    (!!sym(par1)%in%c("H","N") | !!sym(par2)%in%c("H","N")) &
      !!sym(par1)%in%c("H","N") ~ "0",
    ###homozygous 2
    (!!sym(par1)%in%c("H","N") | !!sym(par2)%in%c("H","N")) &
      !!sym(par2)%in%c("H","N") ~ "2"
  )
  )


  #keep positions of markers
  markers <- geno %>% select(marker,chr,!!sym(pos)) %>% distinct()
  markers <- markers %>% arrange(chr,!!sym(pos))


  #keep only interesting columns in geno file
  geno <- geno %>% arrange(chr,!!sym(pos))
  geno <- geno %>% select(marker,id,Geno)

  #remove prefix
  geno <- geno %>% mutate(id=str_remove(id,prefix))

  #keep only non excluded markers and merge with positions
  markers <- markers %>% mutate(marker=as.character(marker))
  markers <- markers %>% mutate(chr=as.character(chr))
  geno <- markers %>% select(marker,chr,!!sym(pos)) %>% full_join(.,geno,by="marker")


  #pivoting
  geno <- geno %>% pivot_wider(names_from = c(marker,chr,!!sym(pos)),values_from = Geno,names_sep=",")
  geno <- geno %>% mutate(id=as.character(id))
  geno <- geno %>% rename("id,,"=id)


  #merge with phenotype file
  pheno <- pheno %>% rename("id"=1)
  pheno <- pheno %>% mutate_all(as.character)
  colnames(pheno) <- str_c(colnames(pheno),",,")
  qtl_file <- right_join(pheno,geno,by=c("id,,"="id,,"))

  #prepare file
  qtl_file <- rbind(colnames(qtl_file),qtl_file)
  qtl_file <- separate_rows(qtl_file,everything(),sep=",")
  colnames(qtl_file) <- qtl_file[1,]
  qtl_file <- qtl_file %>% slice(-1)

  if(is.na(path)==FALSE){
    write.csv(qtl_file,file=path,quote=FALSE,row.names = FALSE)
  }




  return(qtl_file)
}
